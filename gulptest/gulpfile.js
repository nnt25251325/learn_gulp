const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const pug = require('gulp-pug');
const browserSync = require('browser-sync').create();
const cached = require('gulp-cached');

// Option https://pugjs.org/api/reference.html
gulp.task('pug', function() {
  return gulp.src('dev/**/*.pug')
    .pipe(pug({
      doctype: 'html',
      pretty: true
    }))
    .pipe(gulp.dest('dist/'));
});

// https://www.npmjs.com/package/gulp-sass
gulp.task('sass', function() {
  return gulp.src('dev/scss/**/*.scss')
    .pipe(cached('sass'))
    .pipe(
      sass({
        outputStyle: 'expanded',
        indentType: 'space',
        indentWidth: '2'
      }).on('error', sass.logError)
    )
    .pipe(gulp.dest('dist/css/'))
    .pipe(browserSync.stream());
});

gulp.task('default', function() {
  gulp.watch('dev/**/*.pug', gulp.series('pug'));
  gulp.watch('dev/scss/**/*.scss', gulp.series('sass'));
  gulp.watch('dist/**/*.html').on('change', browserSync.reload);
  browserSync.init({
    server: {
      baseDir: './dist'
    }
  })
});
